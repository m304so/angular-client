import { Component } from '@angular/core';
import { Article } from './article';
import { ArticleService } from './article.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Сборник статей';
  articles = [];
  selectedArticle: Article;

  constructor(private apiService: ArticleService){}
  ngOnInit(){
    this.apiService.getArticles().subscribe((res)=> {
      this.articles = res;
    });
  };

  onSelect(article: Article): void {
    this.selectedArticle = article;
  }
}
