import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardComponent } from './dashboard.component';
import { ArticleSearchComponent } from '../article-search/article-search.component';

import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { ARTICLES } from '../mock-articles';
import { ArticleService } from '../article.service';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  let articleService;
  let getArticlesSpy;

  beforeEach(async(() => {
    articleService = jasmine.createSpyObj('ArticleService', ['getArticles']);
    getArticlesSpy = articleService.getArticles.and.returnValue( of(ARTICLES) );
    TestBed.configureTestingModule({
      declarations: [
        DashboardComponent,
        ArticleSearchComponent
      ],
      imports: [
        RouterTestingModule.withRoutes([])
      ],
      providers: [
        { provide: ArticleService, useValue: articleService }
      ]
    })
    .compileComponents();

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should display "Top Articles" as headline', () => {
    expect(fixture.nativeElement.querySelector('h3').textContent).toEqual('Лучшие статьи');
  });

  it('should call articleService', async(() => {
    expect(getArticlesSpy.calls.any()).toBe(true);
    }));

  it('should display 4 links', async(() => {
    expect(fixture.nativeElement.querySelectorAll('a').length).toEqual(4);
  }));

});
