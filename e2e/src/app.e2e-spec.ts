'use strict'; // necessary for es6 output in node

import { browser, element, by, ElementFinder, ElementArrayFinder } from 'protractor';
import { promise } from 'selenium-webdriver';

const expectedH1 = 'Статейник';
const expectedTitle = `${expectedH1}`;
const targetArticle = { id: 15, name: 'Magneta' };
const targetArticleDashboardIndex = 3;
const nameSuffix = 'X';
const newArticleName = targetArticle.name + nameSuffix;

class Article {
  id: number;
  name: string;

  // Factory methods

  // Article from string formatted as '<id> <name>'.
  static fromString(s: string): Article {
    return {
      id: +s.substr(0, s.indexOf(' ')),
      name: s.substr(s.indexOf(' ') + 1),
    };
  }

  // Article from article list <li> element.
  static async fromLi(li: ElementFinder): Promise<Article> {
      let stringsFromA = await li.all(by.css('a')).getText();
      let strings = stringsFromA[0].split(' ');
      return { id: +strings[0], name: strings[1] };
  }

  // Article id and name from the given detail element.
  static async fromDetail(detail: ElementFinder): Promise<Article> {
    // Get article id from the first <div>
    let _id = await detail.all(by.css('div')).first().getText();
    // Get name from the h2
    let _name = await detail.element(by.css('h2')).getText();
    return {
        id: +_id.substr(_id.indexOf(' ') + 1),
        name: _name.substr(0, _name.lastIndexOf(' '))
    };
  }
}

describe('Tutorial part 6', () => {

  beforeAll(() => browser.get(''));

  function getPageElts() {
    let navElts = element.all(by.css('app-root nav a'));

    return {
      navElts: navElts,

      appDashboardHref: navElts.get(0),
      appDashboard: element(by.css('app-root app-dashboard')),
      topArticles: element.all(by.css('app-root app-dashboard > div h4')),

      appArticlesHref: navElts.get(1),
      appArticles: element(by.css('app-root app-articles')),
      allArticles: element.all(by.css('app-root app-articles li')),
      selectedArticleSubview: element(by.css('app-root app-articles > div:last-child')),

      articleDetail: element(by.css('app-root app-article-detail > div')),

      searchBox: element(by.css('#search-box')),
      searchResults: element.all(by.css('.search-result li'))
    };
  }

  describe('Initial page', () => {

    it(`has title '${expectedTitle}'`, () => {
      expect(browser.getTitle()).toEqual(expectedTitle);
    });

    it(`has h1 '${expectedH1}'`, () => {
        expectHeading(1, expectedH1);
    });

    const expectedViewNames = ['Dashboard', 'Articles'];
    it(`has views ${expectedViewNames}`, () => {
      let viewNames = getPageElts().navElts.map((el: ElementFinder) => el.getText());
      expect(viewNames).toEqual(expectedViewNames);
    });

    it('has dashboard as the active view', () => {
      let page = getPageElts();
      expect(page.appDashboard.isPresent()).toBeTruthy();
    });

  });

  describe('Dashboard tests', () => {

    beforeAll(() => browser.get(''));

    it('has top articles', () => {
      let page = getPageElts();
      expect(page.topArticles.count()).toEqual(4);
    });

    it(`selects and routes to ${targetArticle.name} details`, dashboardSelectTargetArticle);

    it(`updates article name (${newArticleName}) in details view`, updateArticleNameInDetailView);

    it(`cancels and shows ${targetArticle.name} in Dashboard`, () => {
      element(by.buttonText('go back')).click();
      browser.waitForAngular(); // seems necessary to gets tests to pass for toh-pt6

      let targetArticleElt = getPageElts().topArticles.get(targetArticleDashboardIndex);
      expect(targetArticleElt.getText()).toEqual(targetArticle.name);
    });

    it(`selects and routes to ${targetArticle.name} details`, dashboardSelectTargetArticle);

    it(`updates article name (${newArticleName}) in details view`, updateArticleNameInDetailView);

    it(`saves and shows ${newArticleName} in Dashboard`, () => {
      element(by.buttonText('save')).click();
      browser.waitForAngular(); // seems necessary to gets tests to pass for toh-pt6

      let targetArticleElt = getPageElts().topArticles.get(targetArticleDashboardIndex);
      expect(targetArticleElt.getText()).toEqual(newArticleName);
    });

  });

  describe('Articles tests', () => {

    beforeAll(() => browser.get(''));

    it('can switch to Articles view', () => {
      getPageElts().appArticlesHref.click();
      let page = getPageElts();
      expect(page.appArticles.isPresent()).toBeTruthy();
      expect(page.allArticles.count()).toEqual(10, 'number of articles');
    });

    it('can route to article details', async () => {
      getArticleLiEltById(targetArticle.id).click();

      let page = getPageElts();
      expect(page.articleDetail.isPresent()).toBeTruthy('shows article detail');
      let article = await Article.fromDetail(page.articleDetail);
      expect(article.id).toEqual(targetArticle.id);
      expect(article.name).toEqual(targetArticle.name.toUpperCase());
    });

    it(`updates article name (${newArticleName}) in details view`, updateArticleNameInDetailView);

    it(`shows ${newArticleName} in Articles list`, () => {
      element(by.buttonText('save')).click();
      browser.waitForAngular();
      let expectedText = `${targetArticle.id} ${newArticleName}`;
      expect(getArticleAEltById(targetArticle.id).getText()).toEqual(expectedText);
    });

    it(`deletes ${newArticleName} from Articles list`, async () => {
      const articlesBefore = await toArticleArray(getPageElts().allArticles);
      const li = getArticleLiEltById(targetArticle.id);
      li.element(by.buttonText('x')).click();

      const page = getPageElts();
      expect(page.appArticles.isPresent()).toBeTruthy();
      expect(page.allArticles.count()).toEqual(9, 'number of articles');
      const articlesAfter = await toArticleArray(page.allArticles);
      // console.log(await Article.fromLi(page.allArticles[0]));
      const expectedArticles =  articlesBefore.filter(h => h.name !== newArticleName);
      expect(articlesAfter).toEqual(expectedArticles);
      // expect(page.selectedArticleSubview.isPresent()).toBeFalsy();
    });

    it(`adds back ${targetArticle.name}`, async () => {
      const newArticleName = 'Alice';
      const articlesBefore = await toArticleArray(getPageElts().allArticles);
      const numArticles = articlesBefore.length;

      element(by.css('input')).sendKeys(newArticleName);
      element(by.buttonText('add')).click();

      let page = getPageElts();
      let articlesAfter = await toArticleArray(page.allArticles);
      expect(articlesAfter.length).toEqual(numArticles + 1, 'number of articles');

      expect(articlesAfter.slice(0, numArticles)).toEqual(articlesBefore, 'Old articles are still there');

      const maxId = articlesBefore[articlesBefore.length - 1].id;
      expect(articlesAfter[numArticles]).toEqual({id: maxId + 1, name: newArticleName});
    });

    it('displays correctly styled buttons', async () => {
      element.all(by.buttonText('x')).then(buttons => {
        for (const button of buttons) {
          // Inherited styles from styles.css
          expect(button.getCssValue('font-family')).toBe('Arial');
          expect(button.getCssValue('border')).toContain('none');
          expect(button.getCssValue('padding')).toBe('5px 10px');
          expect(button.getCssValue('border-radius')).toBe('4px');
          // Styles defined in articles.component.css
          expect(button.getCssValue('left')).toBe('194px');
          expect(button.getCssValue('top')).toBe('-32px');
        }
      });

      const addButton = element(by.buttonText('add'));
      // Inherited styles from styles.css
      expect(addButton.getCssValue('font-family')).toBe('Arial');
      expect(addButton.getCssValue('border')).toContain('none');
      expect(addButton.getCssValue('padding')).toBe('5px 10px');
      expect(addButton.getCssValue('border-radius')).toBe('4px');
    });

  });

  describe('Progressive article search', () => {

    beforeAll(() => browser.get(''));

    it(`searches for 'Ma'`, async () => {
      getPageElts().searchBox.sendKeys('Ma');
      browser.sleep(1000);

      expect(getPageElts().searchResults.count()).toBe(4);
    });

    it(`continues search with 'g'`, async () => {
      getPageElts().searchBox.sendKeys('g');
      browser.sleep(1000);
      expect(getPageElts().searchResults.count()).toBe(2);
    });

    it(`continues search with 'e' and gets ${targetArticle.name}`, async () => {
      getPageElts().searchBox.sendKeys('n');
      browser.sleep(1000);
      let page = getPageElts();
      expect(page.searchResults.count()).toBe(1);
      let article = page.searchResults.get(0);
      expect(article.getText()).toEqual(targetArticle.name);
    });

    it(`navigates to ${targetArticle.name} details view`, async () => {
      let article = getPageElts().searchResults.get(0);
      expect(article.getText()).toEqual(targetArticle.name);
      article.click();

      let page = getPageElts();
      expect(page.articleDetail.isPresent()).toBeTruthy('shows article detail');
      let article2 = await Article.fromDetail(page.articleDetail);
      expect(article2.id).toEqual(targetArticle.id);
      expect(article2.name).toEqual(targetArticle.name.toUpperCase());
    });
  });

  async function dashboardSelectTargetArticle() {
    let targetArticleElt = getPageElts().topArticles.get(targetArticleDashboardIndex);
    expect(targetArticleElt.getText()).toEqual(targetArticle.name);
    targetArticleElt.click();
    browser.waitForAngular(); // seems necessary to gets tests to pass for toh-pt6

    let page = getPageElts();
    expect(page.articleDetail.isPresent()).toBeTruthy('shows article detail');
    let article = await Article.fromDetail(page.articleDetail);
    expect(article.id).toEqual(targetArticle.id);
    expect(article.name).toEqual(targetArticle.name.toUpperCase());
  }

  async function updateArticleNameInDetailView() {
    // Assumes that the current view is the article details view.
    addToArticleName(nameSuffix);

    let page = getPageElts();
    let article = await Article.fromDetail(page.articleDetail);
    expect(article.id).toEqual(targetArticle.id);
    expect(article.name).toEqual(newArticleName.toUpperCase());
  }

});

function addToArticleName(text: string): promise.Promise<void> {
  let input = element(by.css('input'));
  return input.sendKeys(text);
}

function expectHeading(hLevel: number, expectedText: string): void {
    let hTag = `h${hLevel}`;
    let hText = element(by.css(hTag)).getText();
    expect(hText).toEqual(expectedText, hTag);
};

function getArticleAEltById(id: number): ElementFinder {
  let spanForId = element(by.cssContainingText('li span.badge', id.toString()));
  return spanForId.element(by.xpath('..'));
}

function getArticleLiEltById(id: number): ElementFinder {
  let spanForId = element(by.cssContainingText('li span.badge', id.toString()));
  return spanForId.element(by.xpath('../..'));
}

async function toArticleArray(allArticles: ElementArrayFinder): Promise<Article[]> {
  let promisedArticles = await allArticles.map(Article.fromLi);
  // The cast is necessary to get around issuing with the signature of Promise.all()
  return <Promise<any>> Promise.all(promisedArticles);
}
