import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { Article } from './article';
import { MessageService } from './message.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class ArticleService {

  private basUrl = 'http://laravel-api/api';

  constructor(
    private http: HttpClient,
    private messageService: MessageService) { }

  getArticles (): Observable<Article[]> {
    const url = `${this.basUrl}/articles`;
    return this.http.get<Article[]>(url)
      .pipe(
        tap(_ => this.log('fetched articles')),
        catchError(this.handleError('getArticles', []))
      );
  }

  getArticleNo404<Data>(id: number): Observable<Article> {
    const url = `${this.basUrl}/article/?id=${id}`;
    return this.http.get<Article[]>(url)
      .pipe(
        map(articles => articles[0]), // returns a {0|1} element array
        tap(h => {
          const outcome = h ? `fetched` : `did not find`;
          this.log(`${outcome} article id=${id}`);
        }),
        catchError(this.handleError<Article>(`getArticle id=${id}`))
      );
  }

  getArticle(id: number): Observable<Article> {
    const url = `${this.basUrl}/article/${id}`;
    return this.http.get<Article>(url).pipe(
      tap(_ => this.log(`fetched article id=${id}`)),
      catchError(this.handleError<Article>(`getArticle id=${id}`))
    );
  }


  addArticle (article: Article): Observable<Article> {
    const url = `${this.basUrl}/article`;
    return this.http.post<Article>(url, article, httpOptions).pipe(
      tap((article: Article) => this.log(`added article w/ id=${article.id}`)),
      catchError(this.handleError<Article>('addArticle'))
    );
  }

  deleteArticle (article: Article | number): Observable<Article> {
    const id = typeof article === 'number' ? article : article.id;
    const url = `${this.basUrl}/article/${id}`;

    return this.http.delete<Article>(url, httpOptions).pipe(
      tap(_ => this.log(`deleted article id=${id}`)),
      catchError(this.handleError<Article>('deleteArticle'))
    );
  }

  updateArticle (article: Article): Observable<any> {
    const id = typeof article === 'number' ? article : article.id;
    const url = `${this.basUrl}/article/${id}`;
    return this.http.put(url, article, httpOptions).pipe(
      tap(_ => this.log(`updated article id=${article.id}`)),
      catchError(this.handleError<any>('updateArticle'))
    );
  }


  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  private log(message: string) {
    this.messageService.add(`ArticleService: ${message}`);
  }
}
